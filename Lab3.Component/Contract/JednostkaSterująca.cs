﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Contract
{
    public class JednostkaSterująca : ISteruj, IKoła, IOdkurzacz
    {
        Pozycja pozycja;
        Przyciski przyciski;
        Koła koła;
        Odkurzacz odk;
        

        public JednostkaSterująca()
        {
            this.pozycja = new Pozycja();
            this.przyciski = new Przyciski();
            this.odk = new Odkurzacz();
            this.koła = new Koła();
        }

        
        public void Uruchom()
        {
            przyciski.Start();
            pozycja.Wypisz();
            koła.RuszKoła();
            odk.Włącz();
        }

        public void Zatrzymaj()
        {
            przyciski.Stop();
            pozycja.Wypisz();
            koła.ZatrzymajKoła();
            odk.Wyłącz();
        }


        public void RuszKoła()
        {
           
        }

        public void ZatrzymajKoła()
        {
           
        }

        public void Włącz()
        {
           
        }

        public void Wyłącz()
        {
           
        }
    }

    public static class Rozszerzenie
    {
        public static void R(this JednostkaSterująca js)
        {
            Console.WriteLine("rozszerzenie");
        }
    }
}







