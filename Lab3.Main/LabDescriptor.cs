﻿using System;

using Lab3.Contract;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(ISteruj);
        public static Type I2 = typeof(IKoła);
        public static Type I3 = typeof(IOdkurzacz);

        public static Type Component = typeof(JednostkaSterująca);

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Rozszerzenie);
        public static Type MixinFor = typeof(JednostkaSterująca);

        #endregion
    }
}
